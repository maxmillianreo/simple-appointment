<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Appointment extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
			redirect('Auth/Logout');
		}

		$this->load->model('AppointmentModel');
        $this->load->model('DoctorModel');
	}
	
	public function index()
	{
		redirect('Home');
	}

    public function Detail()
    {
        $data['title'] = 'Appointment';

		$appointment = $this->AppointmentModel->Get($this->input->get('id'));

		$data['appointment'] = $appointment;
		$this->load->view('appointment_detail', $data);
    }

    public function Add()
    {
        $data['title'] = 'AppointmentNew';
        $data['list_doctor'] = $this->DoctorModel->GetAll();
        $this->load->view('appointment_add', $data);

    }

    public function doAdd()
    {
        $doctor_id = $this->input->post('doctor_id');
        $appointment_date = $this->input->post('consultation_date');
        $appointment_hour_am = $this->input->post('hour_am');
        $appointment_hour_pm = $this->input->post('hour_pm');
        $appointment_min = $this->input->post('min');
        $appointment_tc = $this->input->post('tc');

        $datetime = date('Y-m-d H:i:s', strtotime($appointment_date . " " . ($appointment_tc == 'AM' ? $appointment_hour_am : $appointment_hour_pm) . ":" . $appointment_min . "" . $appointment_tc));

        $app_id = 'A' . $this->AppointmentModel->GetLastId();
        $data = array(
            'appointment_id' => $app_id,
            'datetime' => $datetime,
            'doctor_id' => $doctor_id,
            'patient_id' => $this->session->userdata('logged_in')->patient_id,
            'status' => 'W',
            'created_at' => NOW,
            'last_update_at' => NOW,
            'last_update_by' => $this->session->userdata('logged_in')->name,
        );
        $this->AppointmentModel->Insert($data);

        $this->session->set_flashdata('info', 'Appointment created successfully.');
        redirect('Home');
    }

    function doCancel()
    {
        $id = $this->input->get('id');
        $update_by = ($this->session->userdata('logged_in')->role == 'D' ? 'Doctor ' : 'Patient ') . $this->session->userdata('logged_in')->name;

        $data = array(
            'status' => 'C',
            'last_update_at' => NOW,
            'last_update_by' => $update_by,
        );
        $this->AppointmentModel->Update($id, $data);

        $this->session->set_flashdata('info', 'Appointment cancelled successfully.');
        redirect('Home');
    }

    function doFix()
    {
        $id = $this->input->get('id');
        $update_by = ($this->session->userdata('logged_in')->role == 'D' ? 'Doctor ' : 'Patient ') . $this->session->userdata('logged_in')->name;

        $data = array(
            'status' => 'F',
            'last_update_at' => NOW,
            'last_update_by' => $update_by,
        );
        $this->AppointmentModel->Update($id, $data);

        $this->session->set_flashdata('info', 'Appointment fixed successfully.');
        redirect('Home');
    }

    function LoadAvaibaleTime()
    {
        $doctor_id = $this->input->post('doctor_id');
        $selected_date = $this->input->post('selected_date');

        $appointment_hour = $this->AppointmentModel->GetAppointmentHour($doctor_id, $selected_date);

        $houram_list_temp = array('08', '09', '10', '11');
        $hourpm_list_temp = array('12', '13', '14', '15');

        foreach($appointment_hour as $hour){
            if (($key = array_search($hour->hour_exist, $houram_list_temp)) !== false) {
                unset($houram_list_temp[$key]);
            }
            if (($key = array_search($hour->hour_exist, $hourpm_list_temp)) !== false) {
                unset($hourpm_list_temp[$key]);
            }
        }

        $houram_list = array();
        foreach($houram_list_temp as $houram){
            array_push($houram_list, $houram);
        }

        $hourpm_list = array();
        foreach($hourpm_list_temp as $hourpm){
            if($hourpm > 12){
                $hourpm = str_pad(strval($hourpm % 12), 2 , '0', STR_PAD_LEFT);
            }
            array_push($hourpm_list, $hourpm);
        }

        $data = array(
            'am' => $houram_list,
            'pm' => $hourpm_list,
        );
        
		$hasil = json_encode($data);
		header('HTTP/1.1: 200');
		header('Status: 200');
		header('Content-Length: '.strlen($hasil));
		exit($hasil);

    }
}
