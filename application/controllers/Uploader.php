<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Uploader extends CI_Controller
{
    public function index()
    {
        $this->load->view('uploader');
    }

    public function doUpload()
    {
        $target_dir = $_SERVER['DOCUMENT_ROOT'] . "/simple-appointment/upload/";
        $target_file = $target_dir . basename($_FILES["file_upload"]["name"]);
        move_uploaded_file($_FILES["file_upload"]["tmp_name"], $target_file);

        $appointment_arr = array();
        ini_set('auto_detect_line_endings', TRUE);
        if (($handle = fopen("upload/example.csv", "r")) !== false) {
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {

                if ($data[0] != '') {
                    $appointment = new stdClass();
                    for ($i = 1; $i <= count($data); $i++) {

                        if ($i % 8 == 1) {
                            $appointment->doctor_id = trim($data[$i - 1]);
                        } elseif ($i % 8 == 2) {
                            $appointment->doctor_name = trim($data[$i - 1]);
                        } elseif ($i % 8 == 3) {
                            $appointment->patient_id = trim($data[$i - 1]);
                        } elseif ($i % 8 == 4) {
                            $appointment->patient_name = trim($data[$i - 1]);
                        } elseif ($i % 8 == 5) {
                            $appointment->patient_age = trim($data[$i - 1]);
                        } elseif ($i % 8 == 6) {
                            $appointment->patient_gender = trim($data[$i - 1]);
                        } elseif ($i % 8 == 7) {
                            $appointment->appointment_id = trim($data[$i - 1]);
                        } elseif ($i % 8 == 0) {
                            $appointment->appointment_datetime = trim($data[$i - 1]);
                        }
                    }
                    $appointment_arr[] = $appointment;
                }
            }
            fclose($handle);

            ## Upload the data to database
            $this->load->model('AppointmentModel');
            $this->AppointmentModel->ImportData($appointment_arr);

            $json_string = json_encode($appointment_arr, JSON_PRETTY_PRINT);

            echo "The data has been saved to database. Click <a href='" . base_url('Welcome') . "'>here</a> to log in.";

            echo "<pre>" . $json_string . "</pre>";
        }
    }
}
