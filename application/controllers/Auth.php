<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('PatientModel');
        $this->load->model('DoctorModel');
        $this->load->helper(array('Form', 'Cookie', 'String'));
    }

    public function index()
    {
        if ($this->session->userdata('logged_in')) {
            redirect('Home');
        } else {
            redirect('Welcome');
        }
    }

    public function Login()
    {
        $userlogin = $this->input->post('username');
        $password = $this->input->post('password');
        $role = $this->input->post('role');

        $doctor_success = $this->DoctorModel->CheckLogin($userlogin, $password);
        $patient_success = $this->PatientModel->CheckLogin($userlogin, $password);

        if ($userlogin == '' or $password == '') {
            $this->session->set_flashdata('error', 'All fields must be filled.');
            redirect('Welcome');
        } else if ($doctor_success == TRUE or $patient_success == TRUE) {
            if ($role == 'D') {
                $userlogin = $this->DoctorModel->GetUserLogin($userlogin, $password);
            } elseif ($role == 'P') {
                $userlogin = $this->PatientModel->GetUserLogin($userlogin, $password);
            } else {
                $this->session->set_flashdata('error', 'Invalid user role.');
                redirect('Welcome');
            }

            $userlogin->role = $role;

            $this->session->set_userdata('logged_in', $userlogin);

            redirect('Home');
        } else {
            $this->session->set_flashdata('error', 'Invalid user ID or password.');
            redirect('Welcome');
        }
    }

    public function Logout()
    {
        $this->session->sess_destroy();
        redirect('Welcome', 'refresh');
    }

    public function ChangePassword()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('Auth/Logout');
        }
        $data['title'] = 'Home';
        $this->load->view('change_password', $data);
    }

    public function doChangePassword()
    {
        if (!$this->session->userdata('logged_in')) {
            redirect('Auth/Logout');
        }

        if ($this->input->post('old_password') == '' or $this->input->post('new_password') == '' or $this->input->post('confirm_password') == '') {
            $this->session->set_flashdata('error', 'All fields must be fiiled.');
            redirect('Auth/ChangePassword');
        } else {
            // check old password
            $old_pass = $this->session->userdata('logged_in')->password;
            if ($old_pass != md5(rtrim($this->input->post('old_password')))) {
                $this->session->set_flashdata('error', 'Invalid old password.');
                redirect('Auth/ChangePassword');
            }

            // cek password baru dengan confirm harus sama
            if (rtrim($this->input->post('new_password')) != rtrim($this->input->post('confirm_password'))) {
                $this->session->set_flashdata('error', 'Invalid password confirmation.');
                redirect('Auth/ChangePassword');
            }

            // update password dan relogin diharuskan
            $data = array(
                'password' => md5(rtrim($this->input->post('new_password')))
            );

            if($this->session->userdata('logged_in')->role == 'D'){
                $this->DoctorModel->UpdateById($this->session->userdata('logged_in')->doctor_id, $data);
            }
            elseif($this->session->userdata('logged_in')->role == 'P'){
                $this->PatientModel->UpdateById($this->session->userdata('logged_in')->patient_id, $data);
            }
            
            $this->session->set_flashdata('info', 'Password updated successfully.');
            redirect('Home');
        }
    }
}
