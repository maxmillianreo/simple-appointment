<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		
		if(!$this->session->userdata('logged_in')){
			redirect('Auth/Logout');
		}

		$this->load->model('AppointmentModel');
	}
	
	public function index()
	{
		$data['title'] = 'Appointment';
		// $this->load->view('dashboard', $data);

		if($this->session->userdata('logged_in')->role == 'D'){
			if($this->input->post('date_filter')){
				$date_filter = $this->input->post('date_filter');
			}
			else{
				$date_filter = '';
			}
		}
		else{
			$date_filter = '';
		}
		
		$data['date_filter'] = $date_filter;
		$list_appointment = $this->AppointmentModel->GetAll($date_filter);

		$data['list_appointment'] = $list_appointment;
		$this->load->view('appointment', $data);
	}
}
