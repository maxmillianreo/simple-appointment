<?php
class PatientModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function CheckId($doctor_id)
	{
		$this->db->where('patient_id', $doctor_id);
		$res = $this->db->get('patient');
		if ($res->num_rows() > 0)
			return true;
		else
			return false;
	}

	function CheckLogin($username, $password)
	{
		$this->db->where('patient_id', $username);
		$this->db->where('password', md5($password));
		$res = $this->db->get('patient');
		if ($res->num_rows() > 0)
			return true;
		else
			return false;
	}

	function GetUserLogin($username, $password)
	{
		$this->db->where('patient_id', $username);
		$this->db->where('password', md5($password));
		$res = $this->db->get('patient');
		// die($this->db->last_query());
		if ($res->num_rows() > 0)
			return $res->row();
		else
			return array();
	}

	function GetAll()
	{

		$qry =	"	
				SELECT 
					*
					
				FROM 
				patient
				ORDER BY name ";

		// die($qry);
		$res = $this->db->query($qry);

		if ($res->num_rows() > 0)
			return $res->result();
		else
			return array();
	}

	function Get($id)
	{
		$res = $this->db->get_where('patient', array('id' => $id));

		if ($res->num_rows() > 0)
			return $res->row();
		else
			return array();
	}

	function GetById($patient_id)
	{
		$res = $this->db->query("SELECT * from patient where patient_id = '" . $patient_id . "'");

		if ($res->num_rows() > 0)
			return $res->row();
		else
			return array();
	}

	function Insert($data)
	{
		$this->db->insert('patient', $data);
	}

	function Update($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('patient', $data);
	}
	function UpdateById($patient_id, $data)
	{
		$this->db->where('patient_id', $patient_id);
		$this->db->update('patient', $data);
	}

	function Delete($id)
	{
		$this->db->delete('patient', array('id' => $id));
	}
}
