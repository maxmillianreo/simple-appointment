<?php
class DoctorModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	function CheckId($doctor_id)
	{
		$this->db->where('doctor_id', $doctor_id);
		$res = $this->db->get('doctor');
		if ($res->num_rows() > 0)
			return true;
		else
			return false;
	}

	function CheckLogin($username, $password)
	{
		$this->db->where('doctor_id', $username);
		$this->db->where('password', md5($password));
		$res = $this->db->get('doctor');
		if ($res->num_rows() > 0)
			return true;
		else
			return false;
	}

	function GetUserLogin($username, $password)
	{
		$this->db->where('doctor_id', $username);
		$this->db->where('password', md5($password));
		$res = $this->db->get('doctor');
		// die($this->db->last_query());
		if ($res->num_rows() > 0)
			return $res->row();
		else
			return array();
	}

	function GetAll()
	{

		$qry =	"	
				SELECT 
					*
					
				FROM 
				doctor
				ORDER BY name ";

		// die($qry);
		$res = $this->db->query($qry);

		if ($res->num_rows() > 0)
			return $res->result();
		else
			return array();
	}

	function Get($id)
	{
		$res = $this->db->get_where('doctor', array('id' => $id));

		if ($res->num_rows() > 0)
			return $res->row();
		else
			return array();
	}

	function GetById($doctor_id)
	{
		$res = $this->db->query("SELECT * from doctor where doctor_id = '" . $doctor_id . "'");

		if ($res->num_rows() > 0)
			return $res->row();
		else
			return array();
	}

	function Insert($data)
	{
		$this->db->insert('doctor', $data);
	}

	function Update($id, $data)
	{
		$this->db->where('id', $id);
		$this->db->update('doctor', $data);
	}
	function UpdateById($doctor_id, $data)
	{
		$this->db->where('doctor_id', $doctor_id);
		$this->db->update('doctor', $data);
	}

	function Delete($id)
	{
		$this->db->delete('doctor', array('id' => $id));
	}
}
