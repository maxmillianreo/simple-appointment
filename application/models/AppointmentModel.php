<?php
class AppointmentModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('DoctorModel');
        $this->load->model('PatientModel');
    }

    function ImportData($data_param)
    {
        $this->db->trans_begin();

        foreach ($data_param as $dat) {
            ### Check if the doctor id already exists in database, if exist, we update the doctor's name, if does not exist, we insert the doctor id and name data
            $doctor_exist = $this->DoctorModel->CheckId($dat->doctor_id);
            if ($doctor_exist) {
                $data = array(
                    'name' => $dat->doctor_name,
                );
                $this->DoctorModel->UpdateById($dat->doctor_id, $data);
            } else {
                $data = array(
                    'doctor_id' => $dat->doctor_id,
                    'password' => 'e10adc3949ba59abbe56e057f20f883e',
                    'name' => $dat->doctor_name,
                );
                $this->DoctorModel->Insert($data);
            }

            ### Check if the patient id already exists in database, if exist, we update the patient name, age and gender. If does not exist, we insert the patient id, name, age and gender data
            $patient_exist = $this->PatientModel->CheckId($dat->patient_id);
            if ($patient_exist) {
                $data = array(
                    'name' => $dat->patient_name,
                    'age' => $dat->patient_age,
                    'gender' => $dat->patient_gender,
                );
                $this->PatientModel->UpdateById($dat->patient_id, $data);
            } else {
                $data = array(
                    'patient_id' => $dat->patient_id,
                    'password' => 'e10adc3949ba59abbe56e057f20f883e',
                    'name' => $dat->patient_name,
                    'age' => $dat->patient_age,
                    'gender' => $dat->patient_gender,
                );
                $this->PatientModel->Insert($data);
            }

            ### Check if the appointment id already exists in database, if exist, we update the appointment datetime. If does not exist, we insert the appointment data
            $appointment_exist = $this->CheckId($dat->appointment_id);
            $appointment_datetime = date('Y-m-d H:i:s', strtotime(
                substr($dat->appointment_datetime, 0, 2) . '-' .
                    substr($dat->appointment_datetime, 2, 2) . '-' .
                    substr($dat->appointment_datetime, 4, 4) . ' ' .
                    substr($dat->appointment_datetime, -8)
            ));

            if ($appointment_exist) {
                $data = array(
                    'datetime' => date('Y-m-d H:i:s', strtotime($appointment_datetime)),
                    'doctor_id' => $dat->doctor_id,
                    'patient_id' => $dat->patient_id,
                    'last_update_by' => 'UPLOAD CSV',
                    'last_update_at' => NOW,
                );
                $this->UpdateById($dat->appointment_id, $data);
            } else {
                $data = array(
                    'appointment_id' => $dat->appointment_id,
                    'datetime' => date('Y-m-d H:i:s', strtotime($appointment_datetime)),
                    'doctor_id' => $dat->doctor_id,
                    'patient_id' => $dat->patient_id,
                    'status' => 'W',
                    'created_at' => NOW,
                    'last_update_by' => 'UPLOAD CSV',
                    'last_update_at' => NOW,
                );
                $this->Insert($data);
            }
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
        } else {
            $this->db->trans_commit();
        }
    }

    function CheckId($appointment_id)
    {
        $this->db->where('appointment_id', $appointment_id);
        $res = $this->db->get('appointment');
        if ($res->num_rows() > 0)
            return true;
        else
            return false;
    }

    function GetAll($date_filter = '')
    {

        $qry =    "	
				SELECT 
                    app.id,
					app.appointment_id,
					app.datetime,
                    app.doctor_id,
                    doc.name as doctor_name,
                    app.patient_id,
                    pat.name as patient_name,
                    pat.age as patient_age,
                    pat.gender as patient_gender,
                    app.status
				FROM 
				appointment app
                INNER JOIN doctor doc on app.doctor_id = doc.doctor_id
                INNER JOIN patient pat on app.patient_id = pat.patient_id
                WHERE 1 = 1 ";

        if ($this->session->userdata('logged_in')->role == 'D') {
            $qry .= " and app.doctor_id = '" . $this->session->userdata('logged_in')->doctor_id . "'";
        } elseif ($this->session->userdata('logged_in')->role == 'P') {
            $qry .= " and app.patient_id = '" . $this->session->userdata('logged_in')->patient_id . "'";
        }

        if ($date_filter != '') {
            $qry .= " and DATE(app.datetime) = '" . date('Y-m-d', strtotime($date_filter)) . "'";
        }

        $qry .= " ORDER BY app.datetime ASC ";

        // die($qry);
        $res = $this->db->query($qry);

        if ($res->num_rows() > 0)
            return $res->result();
        else
            return array();
    }

    function GetLastId()
    {
        $qry = "SELECT MAX(id) as id from appointment ";

        $res = $this->db->query($qry);
        if ($res->num_rows() > 0)
            return ($res->row()->id + 1);
        else
            return array();
    }

    function Get($id)
    {
        $qry = "
            SELECT 
                app.id,
                app.appointment_id,
                app.datetime,
                app.doctor_id,
                doc.name as doctor_name,
                app.patient_id,
                pat.name as patient_name,
                pat.age as patient_age,
                pat.gender as patient_gender,
                app.status,
                app.created_at,
                app.last_update_by,
                app.last_update_at
            FROM 
            appointment app
            INNER JOIN doctor doc on app.doctor_id = doc.doctor_id
            INNER JOIN patient pat on app.patient_id = pat.patient_id
            WHERE app.id = " . $id;

        $res = $this->db->query($qry);
        if ($res->num_rows() > 0)
            return $res->row();
        else
            return array();
    }

    function GetAppointmentHour($doctor_id, $date)
    {
        $qry = "
            SELECT HOUR(datetime) as hour_exist FROM `appointment`  
            where doctor_id = '" . $doctor_id . "'
            and DATE(datetime) = '" . date('Y-m-d', strtotime($date)) . "'
            and status != 'C'
            ORDER BY HOUR(datetime) ASC
        ";
        $res = $this->db->query($qry);
        if ($res->num_rows() > 0)
            return $res->result();
        else
            return array();
    }

    function Insert($data)
    {
        $this->db->insert('appointment', $data);
    }

    function Update($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('appointment', $data);
    }
    function UpdateById($appointment_id, $data)
    {
        $this->db->where('appointment_id', $appointment_id);
        $this->db->update('appointment', $data);
    }

    function Delete($id)
    {
        $this->db->delete('appointment', array('id' => $id));
    }
}
