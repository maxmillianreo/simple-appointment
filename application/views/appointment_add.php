<!DOCTYPE html>
<html>

<?php include '_header.php'; ?>

<body class="bg-default">
    <!-- Sidenav -->
    <?php include '_sidemenu.php'; ?>
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        <?php include '_topmenu.php'; ?>

        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 text-white d-inline-block mb-0"></h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><i class="fas fa-home"></i></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo base_url('Appointment'); ?>">Appointment</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">New</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-6 col-5 text-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <h3 class="mb-0">Make New Appointment</h3>
                        </div>

                        <div class="card-body">

                            <form id="form1" action="<?php echo base_url('Appointment/doAdd'); ?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="doctor_id">Select Doctor</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="ni ni-circle-08"></i>
                                            </span>
                                        </div>
                                        <select class="form-control" id="doctor_id" name="doctor_id" onchange="LoadAvaibaleTime()">
                                            <?php
                                            foreach ($list_doctor as $doctor) {
                                                echo "<option value='" . $doctor->doctor_id . "'>" . $doctor->name . "</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="consultation_date">Date of Consultation</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="ni ni-calendar-grid-58"></i>
                                            </span>
                                        </div>
                                        <input class="form-control datepicker_tanggal" type="text" name="consultation_date" id="consultation_date" value="<?php echo date('d-m-Y', strtotime(NOW . ' +1 days')); ?>" placeholder="Consultation Date" maxlength="10" onchange="LoadAvaibaleTime()">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="consultation_time">Hour of Consultation</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="ni ni-time-alarm"></i>
                                            </span>
                                        </div>
                                        <select class="form-control" name="hour_am" id="hour_am">
                                            <option value="08">08</option>
                                            <option value="09">09</option>
                                            <option value="10">10</option>
                                            <option value="11">11</option>
                                        </select>
                                        <select class="form-control" name="hour_pm" id="hour_pm" style="display: none;">
                                            <option value="12">12</option>
                                            <option value="01">01</option>
                                            <option value="02">02</option>
                                            <option value="03">03</option>
                                        </select>
                                        <select class="form-control" name="min" id="min">
                                            <option value="00">00</option>
                                            <!-- <option value="30">30</option> -->
                                        </select>
                                        <select class="form-control" name="tc" id="tc" onchange="CheckTimeCode(this.value)">
                                            <option value="AM">AM</option>
                                            <option value="PM">PM</option>
                                        </select>
                                    </div>
                                </div>

                                <button class="btn btn-primary btn-sm simpan-button" type="button" onclick="Validate('form1')"><i class="fas fa-save"></i>&nbsp;Submit</button>

                            </form>
                        </div>
                        <div class="card-footer py-4">
                            <nav aria-label="...">

                            </nav>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php include '_footer.php'; ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    function Validate(formid) {
        var submit = confirm("Are you sure want to save this appointment ?");

        if (submit) {
            $('#' + formid).submit();
        }
    }

    function CheckTimeCode(tc) {
        if (tc == 'AM') {
            $('#hour_pm').hide();
            $('#hour_am').show();
        } else if (tc == 'PM') {
            $('#hour_pm').show();
            $('#hour_am').hide();
        }
    }

    function LoadAvaibaleTime() {

        var doctor_id = $('#doctor_id').val();
        var selected_date = $('#consultation_date').val();
        var today = "<?php echo date('d-m-Y', strtotime(NOW)); ?>";

        if(selected_date <= today){
            $('#consultation_date').val('');
            $('#consultation_date').focus();
            alert('The consultation date must be after today.');
        }    

        $.ajax({
            url: "<?php echo base_url('Appointment/LoadAvaibaleTime'); ?>",
            dataType: 'JSON',
            type: 'POST',
            data: {
                doctor_id: doctor_id,
                selected_date: selected_date
            },
            success: function(data) {
                var am = data['am'];
                var pm = data['pm'];

                var am_content = "";
                var pm_content = "";
                for (var i = 0; i < am.length; i++) {
                    am_content += "<option value='" + am[i] + "'>" + am[i] + "</option>";
                }
                for (var j = 0; j < pm.length; j++) {
                    pm_content += "<option value='" + pm[i] + "'>" + pm[j] + "</option>";
                }

                $('#hour_am').html(am_content);
                $('#hour_pm').html(pm_content);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                alert("Data Not Found");
            }
        });

    }
</script>

</html>