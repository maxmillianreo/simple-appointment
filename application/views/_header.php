<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="Simpe Appointment App">
  <meta name="author" content="Reo">
  <title>Simple Appointment App</title>
  <!-- Favicon -->
  <link rel="apple-touch-icon" href="">
  <link rel="shortcut icon" href="">
  <!-- Fonts -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700">
  <!-- Icons -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bootstrap/vendor/nucleo/css/nucleo.css'; ?>" type="text/css">
  <link rel="stylesheet" href="<?php echo base_url() . 'bootstrap/vendor/@fortawesome/fontawesome-free/css/all.min.css'; ?>" type="text/css">
  <!-- Argon CSS -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bootstrap/css/argon.css?v=1.2.1'; ?>" type="text/css">
  <!-- DatePicker CSS  -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bootstrap/datepicker/css/bootstrap-datepicker3.css'; ?>">
  <!-- DataTables CSS  -->
  <link rel="stylesheet" href="<?php echo base_url() . 'bootstrap/css/lib/datatable/dataTables.bootstrap.min.css'; ?>">

  <style>
    .nav-item-child {
      list-style: none;
      height: 0;
      min-height: 0px;
      overflow: hidden !important;
      padding: 0px 1.5rem;
      transition: all 0.5s ease-in-out;
      margin-left: 20px;
    }

    .nav-item-child {
      padding: 0.5rem 1.5rem;
      position: relative;
      height: auto;
      min-height: 50px;
      display: block;
      transition: all 0.2s ease-in-out;
    }

    .dataTables_wrapper .dataTables_filter {
      float: right;
      text-align: right;
      visibility: hidden;
    }

    tbody tr:hover {
      /* cursor: pointer; */
      background-color: #dee6f3;
      color: blue;
      opacity: 0.8;
    }

    .keterangan {
      white-space: normal !important;
      min-width: 190px;
      max-width: 300px;
    }

    .tools {
      min-width: 80px !important;
      max-width: 120px !important;
    }
  </style>
</head>