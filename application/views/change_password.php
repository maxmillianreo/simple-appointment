<!DOCTYPE html>
<html>

<?php include '_header.php'; ?>

<body class="bg-default">
    <!-- Sidenav -->
    <?php include '_sidemenu.php'; ?>
    <!-- Main content -->
    <div class="main-content">
        <!-- Topnav -->
        <?php include '_topmenu.php'; ?>
        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- Page content -->
        <div class="container mt--8 pb-5">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
                    <div class="card bg-secondary border-0 mb-0">

                        <div class="card-body px-lg-5 py-lg-5">
                            <div class="text-center text-muted">
                                <h3>Change Password</h3>
                            </div>
                            <hr>
                            <form role="form" method="post" action="<?php echo base_url('Auth/doChangePassword'); ?>">
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" name="old_password" placeholder="Old Password" type="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" name="new_password" placeholder="New Password" type="password">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control" name="confirm_password" placeholder="Re-type New Password" type="password">
                                    </div>
                                </div>

                                <div class="text-center">
                                    <input type="submit" class="btn btn-primary my-4" value="Submit">
                                </div>
                            </form>
                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <div class="row justify-content-center" style="margin-left:20%;">
        <?php include '_footer.php'; ?>
    </div>
</body>

</html>