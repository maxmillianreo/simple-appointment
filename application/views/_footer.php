<!-- Footer -->
<footer class="py-5" id="footer-main">
  <div class="container">
    <div class="row align-items-center justify-content-xl-between">
      <div class="col-xl-6">
        <div class="copyright text-center text-xl-left text-muted">
          &copy; <?php echo date('Y', strtotime(NOW)); ?> Simple Appointment App</a>
        </div>
      </div>
      
    </div>
  </div>
</footer>
<!-- Argon Scripts -->
<!-- Core -->
<script src="<?php echo base_url() . 'bootstrap/vendor/jquery/dist/jquery.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/vendor/bootstrap/dist/js/bootstrap.bundle.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/vendor/js-cookie/js.cookie.j'; ?>s"></script>
<script src="<?php echo base_url() . 'bootstrap/vendor/jquery.scrollbar/jquery.scrollbar.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js'; ?>"></script>
<!-- Optional JS -->
<script src="<?php echo base_url() . 'bootstrap/vendor/chart.js/dist/Chart.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/vendor/chart.js/dist/Chart.extension.js'; ?>"></script>
<!-- Argon JS -->
<script src="<?php echo base_url() . 'bootstrap/js/argon.js?v=1.2.0'; ?>"></script>
<!-- Datatables JS -->
<script src="<?php echo base_url() . 'bootstrap/js/js_template/lib/data-table/datatables.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/js/js_template/lib/data-table/dataTables.bootstrap.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/js/js_template/lib/data-table/dataTables.buttons.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/js/js_template/lib/data-table/buttons.bootstrap.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/js/js_template/lib/data-table/jszip.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/js/js_template/lib/data-table/vfs_fonts.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/js/js_template/lib/data-table/buttons.html5.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/js/js_template/lib/data-table/buttons.print.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/js/js_template/lib/data-table/buttons.colVis.min.js'; ?>"></script>
<script src="<?php echo base_url() . 'bootstrap/js/js_template/init/datatables-init.js'; ?>"></script>
<!-- DatePicker JS -->
<script src="<?php echo base_url() . 'bootstrap/datepicker/js/bootstrap-datepicker.min.js'; ?>"></script>
<!-- Compressor JS -->
<script src="<?php echo base_url() . 'bootstrap/js/compressor.js'; ?>"></script>



<script type="text/javascript">
  // ### DOCUMENT READY FUNCTION -- EXECUTED WHEN THE PAGE IS ALREADY LOADED
  $(document).ready(function() {
    $('.CapitalText').keyup(function() {
      $(this).val($(this).val().toUpperCase());
    });

    $('.datepicker_tanggal').datepicker({
      format: "dd-mm-yyyy",
      autoclose: true,
    });
  });

  // #################################
  // ### PRE DEFINED HELPER FUNCTION
  // #################################
  function doLogout() {
    var logout = confirm("Are you sure to log out ?");

    if (logout) {
      location.href = "<?php echo base_url('Auth/Logout'); ?>";
    }
  }

  $(".numberFilter").keydown(function(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
      // Allow: Ctrl+A,Ctrl+C,Ctrl+V, Command+A
      ((e.keyCode == 65 || e.keyCode == 86 || e.keyCode == 67) && (e.ctrlKey === true || e.metaKey === true)) ||
      // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
      e.preventDefault();
    }
  });

  function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }

  function numberNoCommas(x) {
    var a = x;
    a = a.replace(/\,/g, ''); // 1125, but a string, so convert it to number
    return parseInt(a, 10);
  }

  $('.search-input-text').on('keyup click', function() { // for text boxes
    var i = $(this).attr('data-column'); // getting column index
    var v = $(this).val(); // getting search input value
    table.columns(i).search(v).draw();
  });

  $('.search-input-select').on('change', function() { // for text boxes
    var i = $(this).attr('data-column'); // getting column index
    var v = $(this).val(); // getting search input value
    table.columns(i).search(v).draw();
  });

  setTimeout(function() {
    $('#error-alert').fadeOut('slow');
  }, 3000);

  setTimeout(function() {
    $('#info-alert').fadeOut('slow');
  }, 3000);
</script>