<!DOCTYPE html>
<html>

<?php include '_header.php'; ?>

<body class="bg-default">
    <!-- Sidenav -->
    <?php include '_sidemenu.php'; ?>
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        <?php include '_topmenu.php'; ?>

        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 text-white d-inline-block mb-0"></h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><i class="fas fa-home"></i></li>
                                    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo base_url('Appointment'); ?>">Appointment</a></li>
                                    <li class="breadcrumb-item active" aria-current="page"><?php echo $appointment->appointment_id; ?></li>
                                </ol>
                            </nav>
                        </div>
                        <div class="col-lg-6 col-5 text-right">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <h3 class="mb-0">Detail Appointment #<?php echo $appointment->appointment_id; ?></h3>
                        </div>

                        <div class="card-body">

                            <div class="form-group">
                                <table class="table">
                                    <tr style="font-weight:bold; background-color:#ffffe1">
                                        <td>Appointment Status</td>
                                        <td>:</td>
                                        <td>
                                            <?php
                                            echo ($appointment->status == 'W' ? 'WAITING' : ($appointment->status == 'F' ? 'FIXED' : ($appointment->status == 'R' ? 'REJECTED' : 'CANCELLED')));

                                            if ($appointment->status != 'W') {
                                                echo ' (by : ' . $appointment->last_update_by . ' at ' . date('d-M-Y H:i:s', strtotime($appointment->last_update_at)) . ')';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Apppointment Date</td>
                                        <td>:</td>
                                        <td><?php echo date('l, d-m-Y', strtotime($appointment->datetime)); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Apppointment Time</td>
                                        <td>:</td>
                                        <td><?php echo date('H:i', strtotime($appointment->datetime)); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Doctor</td>
                                        <td>:</td>
                                        <td><?php echo $appointment->doctor_name . " (" . $appointment->doctor_id . ")"; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Patient's Name</td>
                                        <td>:</td>
                                        <td><?php echo $appointment->patient_name . " (" . $appointment->patient_id . ")"; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Patient's Age</td>
                                        <td>:</td>
                                        <td><?php echo $appointment->patient_age; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Patient's Gender</td>
                                        <td>:</td>
                                        <td><?php echo ($appointment->patient_gender == 'M' ? 'MALE' : 'FEMALE'); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Appointment Created at</td>
                                        <td>:</td>
                                        <td><?php echo date('d-M-Y H:i:s', strtotime($appointment->created_at)); ?></td>
                                    </tr>
                                </table>
                            </div>

                            <div class="form-group">
                                <a href="<?php echo base_url('Appointment'); ?>">
                                    <button class="btn btn-primary btn-sm" type="button"><i class="fas fa-arrow-left"></i>&nbsp;Back to Appointment List</button>
                                </a>
                                <?php if ($this->session->userdata('logged_in')->role == 'D' and $appointment->status == 'W') { ?>
                                    <button class="btn btn-success btn-sm" type="button" onclick="FixAppointment(<?php echo $appointment->id; ?>)"><i class="fas fa-check"></i>&nbsp;Fix Appointment</button>
                                <?php } ?>
                                <?php if ($appointment->status == 'W') { ?>
                                    <button class="btn btn-danger btn-sm" type="button" onclick="CancelAppointment(<?php echo $appointment->id; ?>)"><i class="fas fa-times"></i>&nbsp;Cancel Appointment</button>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="card-footer py-4">
                            <nav aria-label="...">

                            </nav>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php include '_footer.php'; ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    function FixAppointment(id) {
        var submit = confirm("Are you sure want to fix this appointment ?");

        if (submit) {
            window.location.href = "<?php echo base_url('Appointment/doFix?id='); ?>" + id;
        }
    }

    function CancelAppointment(id) {
        var submit = confirm("Are you sure want to cancel this appointment ?");

        if (submit) {
            window.location.href = "<?php echo base_url('Appointment/doCancel?id='); ?>" + id;
        }
    }
</script>

</html>