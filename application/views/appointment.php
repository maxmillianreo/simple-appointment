<!DOCTYPE html>
<html>

<?php include '_header.php'; ?>

<body class="bg-default">
    <!-- Sidenav -->
    <?php include '_sidemenu.php'; ?>
    <!-- Main content -->
    <div class="main-content" id="panel">
        <!-- Topnav -->
        <?php include '_topmenu.php'; ?>

        <!-- Header -->
        <div class="header bg-primary pb-6">
            <div class="container-fluid">
                <div class="header-body">
                    <div class="row align-items-center py-4">
                        <div class="col-lg-6 col-7">
                            <h6 class="h2 text-white d-inline-block mb-0"></h6>
                            <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                                    <li class="breadcrumb-item"><i class="fas fa-home"></i></li>
                                    <li class="breadcrumb-item" aria-current="page">Appointment</li>
                                </ol>
                            </nav>
                        </div>
                        <?php if ($this->session->userdata('logged_in')->role == 'P') { ?>
                            <div class="col-lg-6 col-5 text-right">
                                <a href="<?php echo base_url('Appointment/Add'); ?>" class="btn btn-sm btn-neutral">New Appointment</a>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <!-- Page content -->
        <div class="container-fluid mt--6">
            <div class="row">
                <div class="col">
                    <div class="card">
                        <!-- Card header -->
                        <div class="card-header border-0">
                            <h3 class="mb-0">Appointment List</h3>
                        </div>
                        <div class="card-body">

                            <?php if ($this->session->userdata('logged_in')->role == 'D') { ?>
                            <form id="formFilter" action="<?php echo base_url('Home'); ?>" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="tanggal_mulai" style="font-size:12px">Date Filter</label>
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text" id="basic-addon1">
                                                <i class="ni ni-calendar-grid-58"></i>
                                            </span>
                                        </div>
                                        <input class="form-control datepicker_tanggal" type="text" name="date_filter" id="date_filter" value="<?php echo $date_filter; ?>" placeholder="Date Filter" maxlength="10">
                                        <button class="btn-xs btn-outline-primary" type="submit" onclick="$('#formFilter').submit();">
                                            <i class="fas fa-search"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                            <?php } ?>
                            <div class="form-group table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr bgcolor="#d6e0f0">
                                            <th>Appointment ID</th>
                                            <th>Appointment Time</th>

                                            <?php if ($this->session->userdata('logged_in')->role == 'D') { ?>
                                                <th>Patient's Name</th>
                                                <th>Patient's Age</th>
                                                <th>Patient's Gender</th>
                                            <?php } elseif ($this->session->userdata('logged_in')->role == 'P') { ?>
                                                <th>Doctor's Name</th>
                                            <?php } ?>
                                            <th class="tools"></th>

                                        </tr>
                                    </thead>
                                    <tbody bgcolor="#eef2f9">
                                        <?php
                                        $i = 1;
                                        foreach ($list_appointment as $app) {
                                            echo "<tr>";
                                            echo "<td><b>" . $app->appointment_id . "</b><br>Status : " . ($app->status == 'W' ? 'WAITING' : ($app->status == 'F' ? 'FIXED' : ($app->status == 'R' ? 'REJECTED' : 'CANCELLED'))) . "</td>";
                                            echo "<td>" . date('D, d-m-Y H:i', strtotime($app->datetime)) . "</td>";
                                            if ($this->session->userdata('logged_in')->role == 'D') {
                                                echo "<td class='keterangan'>" . $app->patient_name . "</td>";
                                                echo "<td class='keterangan'>" . $app->patient_age . "</td>";
                                                echo "<td class='transaksi'>" . ($app->patient_gender == 'M' ? 'MALE' : 'FEMALE') . "</td>";
                                            } elseif ($this->session->userdata('logged_in')->role == 'P') {
                                                echo "<td class='keterangan'>" . $app->doctor_name . "</td>";
                                            }
                                            echo "<td>";
                                        ?>
                                        <button class="btn-xs btn-outline-primary" type="button" onclick="Detail(<?php echo $app->id; ?>)">
                                            <i class="fas fa-search"></i>
                                        </button>
                                        <?php if ($this->session->userdata('logged_in')->role == 'D' and $app->status == 'W') { ?>
                                            <button class="btn-xs btn-outline-success" type="button" onclick="FixAppointment(<?php echo $app->id; ?>)">
                                                <i class="fas fa-check"></i>
                                            </button>
                                            
                                            <button class="btn-xs btn-outline-danger" type="button" onclick="CancelAppointment(<?php echo $app->id; ?>)">
                                                <i class="fas fa-times"></i>
                                            </button>
                                        <?php } ?>
                                        <?php
                                            echo "</td>";
                                            echo "</tr>";
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>


                        </div>

                        <div class="card-footer py-4">
                            <nav aria-label="...">

                            </nav>
                        </div>

                    </div>
                </div>
            </div>
            <!-- Footer -->
            <?php include '_footer.php'; ?>
        </div>
    </div>
</body>
<script type="text/javascript">
    function Detail(id) {
        window.location.href = "<?php echo base_url('Appointment/Detail?id='); ?>" + id;
    }

    function FixAppointment(id)
    {
        var submit = confirm("Are you sure want to fix this appointment ?");

        if (submit) {
            window.location.href = "<?php echo base_url('Appointment/doFix?id='); ?>" + id;
        }
    }
    function CancelAppointment(id)
    {
        var submit = confirm("Are you sure want to cancel this appointment ?");

        if (submit) {
            window.location.href = "<?php echo base_url('Appointment/doCancel?id='); ?>" + id;
        }
    }
</script>

</html>