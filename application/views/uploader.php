<!DOCTYPE html>
<html>

<?php include '_header.php'; ?>

<body class="bg-default">
  
  <!-- Main content -->
  <div class="main-content">
    <!-- Header -->
    <div class="header bg-gradient-primary py-4 py-lg-5 pt-lg-2">
      <div class="container">
        <div class="header-body text-center mb-7">
          <div class="row justify-content-center">
            <div class="col-xl-5 col-lg-6 col-md-8 px-5">
                <img class="align-content" src="" alt="" width="">
                <!-- <hr> -->
                <h1 class="text-white">UPLOAD YOUR CSV HERE</h1>
                
            </div>
          </div>
        </div>
      </div>
      
    </div>
    <!-- Page content -->
    <div class="container mt--8 pb-5">
      <div class="row justify-content-center">
        <div class="col-lg-5 col-md-7">
          <div class="card bg-secondary border-0 mb-0">
            
            <div class="card-body px-lg-5 py-lg-5">
              <div class="text-center text-muted mb-4">
                <small>Select your csv file</small>
              </div>
              <form role="form" method="post" action="<?php echo base_url('Uploader/doUpload'); ?>" enctype="multipart/form-data">
                <div class="form-group mb-3">
                  <div class="input-group input-group-merge input-group-alternative">
                    <div class="input-group-prepend">
                      <span class="input-group-text"><i class="ni ni-cloud-upload-96"></i></span>
                    </div>
                    <input class="form-control" type="file" name="file_upload" id="file_upload" accept=".csv">
                  </div>
                </div>
                
                <font color="red">
                  <?php 
                    echo $this->session->flashdata('error'); 
                  ?>
                </font>
                <!-- <div class="custom-control custom-control-alternative custom-checkbox">
                  <input class="custom-control-input" id=" customCheckLogin" type="checkbox">
                  <label class="custom-control-label" for=" customCheckLogin">
                    <span class="text-muted">Remember me</span>
                  </label>
                </div> -->
                <div class="text-center">
                  <input type="submit" class="btn btn-primary my-4" value="Submit">
                </div>
              </form>
            </div>
          </div>
          <!-- <div class="row mt-3">
            <div class="col-6">
              <a href="#" class="text-light"><small>Forgot password?</small></a>
            </div>
            <div class="col-6 text-right">
              <a href="#" class="text-light"><small>Create new account</small></a>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  
  <?php include '_footer.php'; ?>
</body>

</html>