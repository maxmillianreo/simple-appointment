<div style="display:<?php if (!$this->session->flashdata('info')) echo 'none'; ?>; position:absolute; top:0; left:0; width: 100%; z-index: 99999 !important;" id="info-alert" class="alert alert-success col-sm-12">
    <!-- info msg here -->
    <?php
    echo $this->session->flashdata('info');
    ?>
</div>

<div style="display:<?php if (!$this->session->flashdata('error')) echo 'none'; ?>; position:absolute; top:0; left:0; width: 100%; z-index: 99999 !important; font-family:Arial" id="error-alert" class="alert alert-danger col-sm-12">
    <!-- error msg here -->
    <?php
    echo $this->session->flashdata('error');
    ?>
</div>
<nav class="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
        <!-- Brand -->
        <div class="sidenav-header  align-items-center">
            <a class="navbar-brand" href="javascript:void(0)">
                <img src="<?php echo base_url('assets/img/logo.png'); ?>" class="navbar-brand-img" alt="...">&nbsp;
                <!-- <b><?php echo $this->session->userdata('logged_in')->role; ?></b> -->
            </a>
        </div>
        <div class="navbar-inner">
            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                
                <!-- Heading -->
                <h6 class="navbar-heading p-0 text-muted">
                    <span class="docs-normal">Appointment</span>
                </h6>
                <!-- Navigation -->
                <ul class="navbar-nav mb-md-3">
                    <!-- Nested Menu  -->
                    <li class="nav-item">
                        <a class="nav-link <?php echo ($title == 'Appointment' ? 'active' : ''); ?>" href="<?php echo base_url('Appointment'); ?>">
                            <i class="ni ni-calendar-grid-58 text-primary"></i>
                            <span class="nav-link-text">My Appointment</span>
                        </a>
                    </li>

                    <?php if($this->session->userdata('logged_in')->role == 'P') { ?>
                    <li class="nav-item">
                        <a class="nav-link <?php echo ($title == 'AppointmentNew' ? 'active' : ''); ?>" href="<?php echo base_url('Appointment/Add'); ?>">
                            <i class="ni ni-send text-primary"></i>
                            <span class="nav-link-text">Make Appointment</span>
                        </a>
                    </li>
                    <?php } ?>
                </ul>
                
            </div>
        </div>
    </div>
</nav>