-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2021 at 06:08 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_appointment`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` int(11) NOT NULL,
  `appointment_id` char(10) NOT NULL,
  `datetime` datetime NOT NULL,
  `doctor_id` char(10) NOT NULL,
  `patient_id` char(10) NOT NULL,
  `status` char(1) NOT NULL,
  `created_at` datetime NOT NULL,
  `last_update_by` varchar(200) NOT NULL,
  `last_update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `appointment_id`, `datetime`, `doctor_id`, `patient_id`, `status`, `created_at`, `last_update_by`, `last_update_at`) VALUES
(1, 'A1', '2018-03-08 09:00:00', 'D1', 'P1', 'W', '2021-07-11 12:08:12', 'UPLOAD CSV', '2021-07-11 12:08:12'),
(2, 'A2', '2018-04-08 10:00:00', 'D1', 'P1', 'W', '2021-07-11 12:08:12', 'UPLOAD CSV', '2021-07-11 12:08:12'),
(3, 'A3', '2018-03-08 10:00:00', 'D1', 'P2', 'W', '2021-07-11 12:08:12', 'UPLOAD CSV', '2021-07-11 12:08:12'),
(4, 'A4', '2018-04-08 11:00:00', 'D1', 'P1', 'W', '2021-07-11 12:08:12', 'UPLOAD CSV', '2021-07-11 12:08:12'),
(5, 'A5', '2018-03-18 08:00:00', 'D2', 'P1', 'W', '2021-07-11 12:08:12', 'UPLOAD CSV', '2021-07-11 12:08:12'),
(6, 'A6', '2018-04-18 09:00:00', 'D2', 'P1', 'W', '2021-07-11 12:08:12', 'UPLOAD CSV', '2021-07-11 12:08:12'),
(7, 'A7', '2018-03-18 09:00:00', 'D2', 'P3', 'W', '2021-07-11 12:08:12', 'UPLOAD CSV', '2021-07-11 12:08:12'),
(8, 'A8', '2018-04-18 10:00:00', 'D2', 'P3', 'W', '2021-07-11 12:08:12', 'UPLOAD CSV', '2021-07-11 12:08:12'),
(9, 'A9', '2021-07-12 09:00:00', 'D1', 'P2', 'C', '2021-07-11 17:28:21', 'Patient P2Name', '2021-07-11 17:42:49'),
(15, 'A15', '2021-07-13 13:00:00', 'D1', 'P2', 'F', '2021-07-11 17:28:36', 'Doctor D1Name', '2021-07-12 10:52:00'),
(16, 'A16', '2021-07-12 08:00:00', 'D1', 'P2', 'W', '2021-07-11 22:38:52', 'P2Name', '2021-07-11 22:38:52'),
(17, 'A17', '2021-07-13 08:00:00', 'D1', 'P2', 'C', '2021-07-12 10:32:29', 'Doctor D1Name', '2021-07-12 10:40:53'),
(18, 'A18', '2021-07-13 08:00:00', 'D1', 'P1', 'W', '2021-07-12 10:41:56', 'P1Name', '2021-07-12 10:41:56');

-- --------------------------------------------------------

--
-- Table structure for table `doctor`
--

CREATE TABLE `doctor` (
  `id` int(11) NOT NULL,
  `doctor_id` char(10) NOT NULL,
  `password` char(32) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `doctor`
--

INSERT INTO `doctor` (`id`, `doctor_id`, `password`, `name`) VALUES
(1, 'D1', 'e10adc3949ba59abbe56e057f20f883e', 'D1Name'),
(2, 'D2', 'e10adc3949ba59abbe56e057f20f883e', 'D2Name');

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(11) NOT NULL,
  `patient_id` char(10) NOT NULL,
  `password` char(32) NOT NULL,
  `name` varchar(200) NOT NULL,
  `age` decimal(10,0) NOT NULL,
  `gender` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `patient_id`, `password`, `name`, `age`, `gender`) VALUES
(1, 'P1', 'e10adc3949ba59abbe56e057f20f883e', 'P1Name', '12', 'M'),
(2, 'P2', 'e10adc3949ba59abbe56e057f20f883e', 'P2Name', '22', 'F'),
(3, 'P3', 'e10adc3949ba59abbe56e057f20f883e', 'P3Name', '32', 'M');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctor`
--
ALTER TABLE `doctor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `doctor`
--
ALTER TABLE `doctor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
